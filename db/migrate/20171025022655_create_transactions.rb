class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.boolean :deposit, default: false
      t.decimal :value_deposit, precision: 10, default: 0, scale: 2
      t.boolean :transfer, default: false
      t.string :value_transfer, precision: 10, default: 0, scale: 2
      t.boolean :withdrawal, default: false
      t.string :value_withdrawal, precision: 10, default: 0, scale: 2
      t.decimal :fee_from_transaction, precision: 10, default: 0, scale: 2
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
