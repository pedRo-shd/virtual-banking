class CreateWithdrawals < ActiveRecord::Migration[5.1]
  def change
    create_table :withdrawals do |t|
      t.string :name
      t.string :account_number, null: false
      t.decimal :value, precision: 10, default: 0, scale: 2, null: false
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
