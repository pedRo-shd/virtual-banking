class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.decimal :balance, precision: 10, default: 0, scale: 2, null: false
      t.boolean :active, null: false
      t.string :number_from_account, null: false
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
