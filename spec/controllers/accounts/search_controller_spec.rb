require 'rails_helper'

RSpec.describe Accounts::SearchController, type: :controller do

  describe "GET #account_extract" do
    it "returns http success" do
      get :account_extract
      expect(response).to have_http_status(:success)
    end
  end

end
