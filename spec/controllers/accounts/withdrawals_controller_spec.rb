require 'rails_helper'

RSpec.describe Accounts::WithdrawalsController, type: :controller do

  describe "GET #new" do
    it "returns http success" do
      @user = create(:user)
      sign_in @user
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      before  do
        @user = create(:user)
        @account = create(:account, user_id: @user.id,
                                    number_from_account: '122',
                                    balance: 5000,
                                    active: true)
        sign_in @user
      end

      it 'must create a Withdrawal from account' do
        post :create, params: { withdrawal: { account_number: @account.number_from_account,
                                              value: 50 } }
        expect(flash[:notice]).to match(/Saque efetuado com sucesso.*/)
      end
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      before  do
        @account2 = create(:account)
        @user = create(:user)
        @account  = create(:account, user_id: @user.id)
        sign_in @user
      end

      it 'must not create a Withdrawal from account with same user' do
        post :create, params: { withdrawal: { account_number: @account.number_from_account,
                                              value: 50 } }
        expect(Withdrawal.all.count).to eql(0)
      end

      it 'redirect to #home' do
        post :create, params: { withdrawal: { account_number: @account.number_from_account,
                                              value: 6000 } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Saque não efetuado, tente novamente!.*/)
      end
    end

    context 'with not valid user' do
      before do
        @account  = create(:account)
      end

      it 'must not create a withdrawal' do
        post :create, params: { withdrawal: { account_number: nil,
                                              value: 6000 } }
        expect(Withdrawal.all.count).to eql(0)
      end

      it 'must be redirect to sign in' do
        post :create, params: { withdrawal:
                                { account_number: @account.number_from_account,
                                  value: 6000 } }
        expect(response).to redirect_to new_user_session_path
        expect(flash[:alert]).to match(/Você precisa logar ou cadastrar antes de continuar.*/)
      end
    end

    context 'with not valid params' do
      before do
        @user = create(:user)
        sign_in @user
      end

      it 'must not create a withdrawal without an account' do
        post :create, params: { withdrawal: { account_number: nil,
                                              value: 6000 } }
        expect(Withdrawal.all.count).to eql(0)
      end

      it 'redirect to with alert without an account number' do
        post :create, params: { withdrawal: { account_number: nil,
                                              value: 6000 } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Saque não efetuado, tente novamente!*/)
      end

      it 'must not create a withdrawal without an account number and value' do
        post :create, params: { withdrawal: { account_number: nil,
                                              value: nil } }
        expect(Withdrawal.all.count).to eql(0)
      end

      it 'redirect to with alert without an account number and value' do
        post :create, params: { withdrawal: { account_number: nil,
                                              value: nil } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Saque não efetuado, tente novamente!.*/)
      end

      it 'must not create a withdrawal with invalid account number' do
        @account = create(:account, number_from_account: '155-20-30',
                                    active: true)
        post :create, params: { withdrawal: { account_number: '155-20-31',
                                           value: 600 } }
        expect(Withdrawal.all.count).to eql(0)
      end

      it 'redirect to with alert with invalid account number' do
        @account = create(:account, number_from_account: '155-20-30',
                                    active: true)
        post :create, params: { withdrawal: { account_number: '155-20-31',
                                              value: 600 } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Saque não efetuado, tente novamente!.*/)
      end

      it 'redirect to with alert with invalid account number without value'  do
        @account = create(:account, number_from_account: '155-20-30',
                                    active: true)
        post :create, params: { withdrawal: { account_number: '155-20-31',
                                              value: nil } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Saque não efetuado, tente novamente!.*/)
      end
    end
  end

  describe "GET #show" do
    it "returns http success" do
      @user = create(:user)
      sign_in @user
      @withdrawal = create(:withdrawal)
      get :show, params: { id: @withdrawal.id }
      expect(response).to have_http_status(:success)
    end
  end

end
