require 'rails_helper'

RSpec.describe Accounts::TransfersController, type: :controller do

  describe "GET #new" do
    it "returns http success" do
      @user = create(:user)
      sign_in @user
    end
  end

  describe "POST #create" do
    context 'with valid params in week days between 9 at 18 hours' do
      before  do
        @user = create(:user)
        sign_in @user
        @account = create(:account, number_from_account: '22-355',
                                    balance: 1030,
                                    user_id: @user.id)
        @account1 = create(:account)
        Time.stub(:now).and_return(Time.mktime(2017,10,23,9,00,01))
      end

      it "subtract 15 of balance of the current user when transfer less than 1000" do
        post :create, params: { transfer: { account_number: @account1.number_from_account,
                                            value: 10 } }
        @account.reload
        expect(@account.balance).to eql(1015)
        expect(response).to redirect_to accounts_transfer_path(Transfer.last.id)
        expect(flash[:notice]).to match(/Transferência efetuado com sucesso.*/)
      end

      it "subtract 1015 of balance of the current when transfer greather than 1000" do
        post :create, params: { transfer: { account_number: @account1.number_from_account,
                                            value: 1000 } }
        @account.reload
        expect(@account.balance).to eql(15)
        expect(response).to redirect_to accounts_transfer_path(Transfer.last.id)
        expect(flash[:notice]).to match(/Transferência efetuado com sucesso.*/)
      end
    end

    context 'with valid params in week days different from 9 at 18 hours' do
      before  do
        @user = create(:user)
        sign_in @user
        @account = create(:account, number_from_account: '22-355',
                                    balance: 1020,
                                    user_id: @user.id)
        @account1 = create(:account)
      end

      it "subtract 7 of balance of the current user when transfer less than 1000" do
        Time.stub(:now).and_return(Time.mktime(2017,10,23,8,59,01))

        post :create, params: { transfer: { account_number: @account1.number_from_account,
                                            value: 10 } }

        @account.reload
        expect(@account.balance).to eql(1003)
        expect(response).to redirect_to accounts_transfer_path(Transfer.last.id)
        expect(flash[:notice]).to match(/Transferência efetuado com sucesso.*/)
      end

      it "subtract 7 of balance of the current user when transfer less than 1000" do
        Time.stub(:now).and_return(Time.mktime(2017,10,21,8,59,01))
        post :create, params: { transfer: { account_number: @account1.number_from_account,
                                            value: 10 } }

        @account.reload
        expect(@account.balance).to eql(1003)
        expect(response).to redirect_to accounts_transfer_path(Transfer.last.id)
        expect(flash[:notice]).to match(/Transferência efetuado com sucesso.*/)
      end

      it "subtract 17 of balance of the current when transfer greather than 1000" do
        Time.stub(:now).and_return(Time.mktime(2017,10,23,8,59,01))
        post :create, params: { transfer: { account_number: @account1.number_from_account,
                                            value: 1000 } }
        @account.reload
        expect(@account.balance).to eql(3)
        expect(response).to redirect_to accounts_transfer_path(Transfer.last.id)
        expect(flash[:notice]).to match(/Transferência efetuado com sucesso.*/)
      end

      it "subtract 17 of balance of the current when transfer greather than 1000" do
        Time.stub(:now).and_return(Time.mktime(2017,10,21,10,59,01))
        post :create, params: { transfer: { account_number: @account1.number_from_account,
                                            value: 1000 } }
        @account.reload
        expect(@account.balance).to eql(3)
        expect(response).to redirect_to accounts_transfer_path(Transfer.last.id)
        expect(flash[:notice]).to match(/Transferência efetuado com sucesso.*/)
      end
    end


    context 'with not valid user' do
      before do
        @account  = create(:account)
      end

      it 'must not create a withdrawal' do
        post :create, params: { transfer: { account_number: nil,
                                              value: 6000 } }
        expect(Transfer.all.count).to eql(0)
      end

      it 'must be redirect to sign in' do
        post :create, params: { transfer:
                                { account_number: @account.number_from_account,
                                  value: 6000 } }
        expect(response).to redirect_to new_user_session_path
        expect(flash[:alert]).to match(/Você precisa logar ou cadastrar antes de continuar.*/)
      end
    end

    context 'with not valid params' do
      before do
        @user = create(:user)
        sign_in @user
      end

      it 'must not create a withdrawal without an account' do
        post :create, params: { transfer: { account_number: nil,
                                              value: 6000 } }
        expect(Transfer.all.count).to eql(0)
      end

      it 'redirect to with alert without an account number' do
        post :create, params: { transfer: { account_number: nil,
                                              value: 6000 } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Transferência não efetuado, tente novamente!.*/)
      end

      it 'must not create a withdrawal without an account number and value' do
        post :create, params: { transfer: { account_number: nil,
                                              value: nil } }
        expect(Transfer.all.count).to eql(0)
      end

      it 'redirect to with alert without an account number and value' do
        post :create, params: { transfer: { account_number: nil,
                                              value: nil } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Transferência não efetuado, tente novamente!.*/)
      end

      it 'must not create a withdrawal with invalid account number' do
        @account = create(:account, number_from_account: '155-20-30',
                                    active: true)
        post :create, params: { transfer: { account_number: '155-20-31',
                                           value: 600 } }
        expect(Transfer.all.count).to eql(0)
      end

      it 'redirect to with alert with invalid account number' do
        @account = create(:account, number_from_account: '155-20-30',
                                    active: true)
        post :create, params: { transfer: { account_number: '155-20-31',
                                              value: 600 } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Transferência não efetuado, tente novamente!.*/)
      end

      it 'redirect to with alert with invalid account number without value'  do
        @account = create(:account, number_from_account: '155-20-30',
                                    active: true)
        post :create, params: { transfer: { account_number: '155-20-31',
                                              value: nil } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Transferência não efetuado, tente novamente!.*/)
      end
    end
  end

  describe "GET #show" do
    it "returns http success" do
      @user = create(:user)
      sign_in @user
      @transfer = create(:transfer)
      get :show, params: { id: @transfer.id }
      expect(response).to have_http_status(:success)
    end
  end
end
