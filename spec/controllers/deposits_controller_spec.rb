require 'rails_helper'

RSpec.describe DepositsController, type: :controller do
  describe 'POST #create' do
    context 'with valid params' do
      before  do
        @account  = create(:account)
        @account2 = create(:account)
      end

      it 'create a deposit with an account' do
        post :create, params: { deposit: { account_number: @account.number_from_account,
                                           value: 6000 } }
        expect(Deposit.all.count).to eql(1)
      end

      it 'create a deposit with an account' do
        post :create, params: { deposit: { account_number: @account2.number_from_account,
                                           value: 1500 } }
        expect(Deposit.all.count).to eql(1)
      end

      it 'redirect to with notice' do
        post :create, params: { deposit: { account_number: @account.number_from_account,
                                           value: 6000 } }
        expect(response).to redirect_to deposit_path(Deposit.last.id)
        expect(flash[:notice]).to match(/Depósito efetuado com sucesso.*/)
      end
    end


    context 'with not valid params' do
      it 'must not create a deposit without an account' do
        post :create, params: { deposit: { account_number: nil,
                                           value: 6000 } }
        expect(Deposit.all.count).to eql(0)
      end

      it 'redirect to with alert without an account number' do
        post :create, params: { deposit: { account_number: nil,
                                           value: 6000 } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Deposito não efetuado, tente novamente!.*/)
      end

      it 'must not create a deposit without an account number and value' do
        post :create, params: { deposit: { account_number: nil,
                                           value: nil } }
        expect(Deposit.all.count).to eql(0)
      end

      it 'redirect to with alert without an account number and value' do
        post :create, params: { deposit: { account_number: nil,
                                           value: nil } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Deposito não efetuado, tente novamente!.*/)
      end

      it 'must not create a deposit with invalid account number' do
        @account = create(:account, number_from_account: '155-20-30',
                                    active: true)
        post :create, params: { deposit: { account_number: '155-20-31',
                                           value: 600 } }
        expect(Deposit.all.count).to eql(0)
      end

      it 'redirect to with alert with invalid account number' do
        @account = create(:account, number_from_account: '155-20-30',
                                    active: true)
        post :create, params: { deposit: { account_number: '155-20-31',
                                           value: 600 } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Deposito não efetuado, tente novamente!.*/)
      end

      it 'redirect to with alert with invalid account number without value'  do
        @account = create(:account, number_from_account: '155-20-30',
                                    active: true)
        post :create, params: { deposit: { account_number: '155-20-31',
                                           value: nil } }
        expect(response).to redirect_to '/'
        expect(flash[:alert]).to match(/Deposito não efetuado, tente novamente!.*/)
      end
    end
  end
end
