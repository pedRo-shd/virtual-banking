FactoryGirl.define do
  factory :profle_user do
    full_name FFaker::NameBR.name
    born_at Time.now - 20.year
    user
  end
end
