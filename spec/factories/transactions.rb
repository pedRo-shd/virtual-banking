FactoryGirl.define do
  factory :transaction do
    deposit false
    value_deposit "9.99"
    transfer false
    value_transfer "MyString"
    withdrawal false
    value_withdrawal "MyString"
    account nil
  end
end
