FactoryGirl.define do
  factory :account do
    balance FFaker.numerify("#.##").to_f
    active FFaker::Boolean::maybe
    number_from_account { FFaker::AddressKR.land_number }
    user
  end
end
