FactoryGirl.define do
  factory :transfer do
    name { FFaker::NameBR.name }
    account_number FFaker::AddressKR.land_number
    value FFaker.numerify("#.##").to_f
    account
  end
end
