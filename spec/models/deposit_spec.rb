require 'rails_helper'

RSpec.describe Deposit, type: :model do

  it 'belongs to account' do
    assc = described_class.reflect_on_association(:account)
    expect(assc.macro).to eq :belongs_to
  end

  it 'is not valid without valid attributes' do
    expect(Deposit.new).to_not be_valid
  end

  it 'is valid with valid attributes' do
    account = create(:account)
    expect(Deposit.new(account_number: '112-33-2211',
                       value: 1800,
                       account_id: account.id)).to be_valid
  end

  it "is not valid without account number" do
    deposit = Deposit.new(account_number: nil)
    expect(deposit).to_not be_valid
  end

  it "is valid with valid account number and account_id, value is 0 for default" do
    account = create(:account)
    expect(Deposit.new(account_number: '122-55-3302',
                       account_id: account.id)).to be_valid
  end

  it "is not valid with valid account number but with account id" do
    expect(Deposit.new(account_number: '122-55-3302',
                       account_id: nil)).to_not be_valid
  end

  it "is not valid with account id but without account number" do
    account = create(:account)
    expect(Deposit.new(account_number: nil,
                       account_id: account.id)).to_not be_valid
  end

  it "is not valid without account number" do
    deposit = Deposit.new(value: nil)
    expect(deposit).to_not be_valid
  end

  it "is not valid with valid value and account number but without account id" do
    expect(Deposit.new(account_number: '122-55-3302',
                       value: 200)).to_not be_valid
  end

  it "is not valid with valid attributes if value is nil" do
    account = create(:account)
    expect(Deposit.new(account_number: '122-55-3302',
                       value: nil,
                       account_id: account.id)).to_not be_valid
  end

  it "is valid with value > 0 and valid attributes" do
    account = create(:account)
    expect(Deposit.new(account_number: '122-55-3302',
                       value: 500,
                       account_id: account.id)).to be_valid
  end

  it "is not valid with valid account number but with account id nil" do
    expect(Deposit.new(account_number: '122-55-3302',
                       account_id: nil)).to_not be_valid
  end

  it "is not valid with value but account id nil" do
    expect(Deposit.new(value: 200,
                       account_id: nil)).to_not be_valid
  end
end
