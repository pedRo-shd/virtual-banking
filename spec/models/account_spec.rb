require 'rails_helper'

RSpec.describe Account, type: :model do

  it 'belongs to user' do
    assc = described_class.reflect_on_association(:user)
    expect(assc.macro).to eq :belongs_to
  end

  it 'has many to deposits' do
    assc = described_class.reflect_on_association(:deposits)
    expect(assc.macro).to eq :has_many
  end

  it 'has many to withdrawals' do
    assc = described_class.reflect_on_association(:withdrawals)
    expect(assc.macro).to eq :has_many
  end

  it 'has many to transfers' do
    assc = described_class.reflect_on_association(:transfers)
    expect(assc.macro).to eq :has_many
  end

  it 'is not valid without valid attributes' do
    expect(Account.new).to_not be_valid
  end

  it 'is valid with valid attributes' do
    @user = create(:user)
    expect(Account.new(balance: 0,
                       active: true,
                       number_from_account: '112-33-2211',
                       user_id: @user.id)).to be_valid
  end

  it "is not valid without number from account" do
    @user = create(:user)
    account = Account.new(balance: 0,
                          active: true,
                          number_from_account: '',
                          user_id: @user.id)
    expect(account).to_not be_valid
  end

  it "is valid without accout active" do
    @user = create(:user)
    account = Account.new(balance: 0,
                          active: false,
                          number_from_account: '112-33-2211',
                          user_id: @user.id)
    expect(account).to be_valid
  end

  it "is not valid with double account" do
    @user    = create(:user)
    @account = create(:account, number_from_account: '112-33-2211')
    account  = Account.new(balance: 0,
                           active: true,
                           number_from_account: '112-33-2211',
                           user_id: @user.id)
    expect(account).to_not be_valid
  end

  it "is not valid without user" do
    account  = Account.new(balance: 0,
                           active: true,
                           number_from_account: '112-33-2211',
                           user_id: '')
    expect(account).to_not be_valid
  end

  it "is not valid without user" do
    account  = Account.new(balance: 0,
                           active: true,
                           number_from_account: '112-33-2211',
                           user_id: '')
    expect(account).to_not be_valid
  end

  context 'with account inactive' do
    before do
      @account = create(:account, number_from_account: '113-10',
                                  active: false,
                                  balance: 10)
      @obj    = create(:deposit)
      @obj2   = create(:transfer)
      @number = @account.number_from_account
      @value  = 500
    end

    it 'must not have deposits with account inactive' do
      account = Account.to_find_acount(@obj, @number, @value)
      account.reload
      expect(account.balance).to eql(10)
    end

    it 'must not have transfers with account inactive' do
      account = Account.to_find_acount(@obj2, @number, @value)
      account.reload
      expect(account.balance).to eql(10)
    end
  end

  context 'with account active' do
    before do
      @account = create(:account, number_from_account: '113-10',
                                  active: true  ,
                                  balance: 10)
      @obj    = create(:deposit)
      @obj2   = create(:transfer)
      @number = @account.number_from_account
      @value  = 500
    end

    it 'must have deposits with account active' do
      account = Account.to_find_acount(@obj, @number, @value)
      account.reload
      expect(account.balance).to eql(510)
    end

    it 'must not have transfers with account active' do
      account = Account.to_find_acount(@obj2, @number, @value)
      account.reload
      expect(account.balance).to eql(510)
    end
  end
end
