Rails.application.routes.draw do

  namespace :accounts do
    get 'search', to: 'search#account_extract'
  end

  namespace :accounts do
    resources :accounts, only: %i[index new edit create update show]
    get 'balance_account', to: 'accounts#balance_account'
    resources :profile_users, only: %i[edit update]
    resources :withdrawals, only: %i[new create show]
    resources :transfers, only: %i[new create show]
  end

  resources :deposits, only: %i[new create show]

  devise_for :users

  root 'home#index'
end
