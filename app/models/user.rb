class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_one :account
  has_many :transfers
  has_many :deposits
  has_many :withdrawals
  
  # accepts_nested_attributes_for :profile_user
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Validations
  # validate :nested_attributes
  #
  # def nested_attributes
  #   return unless nested_attributes_blank?
  #   errors.add(:base, 'É necessário preencher o campo nome.')
  # end
  #
  # def nested_attributes_blank?
  #   profile_user.full_name.blank?
  # end
end
