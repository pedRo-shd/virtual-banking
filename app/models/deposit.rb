class Deposit < ApplicationRecord
  belongs_to :account
  has_many :transfers, through: :account

  validates :account_number, presence: true
  validates :value, presence: true
  validates :account_id, presence: true
end
