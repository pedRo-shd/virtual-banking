class Account < ApplicationRecord
  belongs_to :user
  has_many :deposits
  has_many :withdrawals
  has_many :transfers

  scope :get_of_balance, ->(current_user_id) { where(user_id: current_user_id).first }
  # scope :search, -> (q, term, page) { where("lower(created_at) LIKE ?", "%#{q.downcase}%").page(page).per(QT_PAGE_PER)}
  validates :number_from_account, presence: true, uniqueness: true
  validates :balance, presence: true
  validate  :balance_negative?
  validate  :only_one_user


  def self.to_find_acount(obj, number, value, current_user)
    account = self.where(number_from_account: number).last
    return unless account
    if obj.class == Transfer
      return nil unless not_have_balance(current_user, value, number)
      account.balance += value
    end
    account.balance += value if obj.class == Deposit
    account.balance -= value if obj.class == Withdrawal
    return nil unless account.balance > 0
    account = account.active == true ? account : nil
    account.save if account.present?
    account
  end

  private

  def balance_negative?
    return unless balance < 0
    errors.add(:balance, 'da sua conta não pode ser negativo')
    throw(:abort)
  end

  def only_one_user
    account = Account.where(user_id: user_id).first
    return unless account
    if self.id.blank? && self.user_id == account.user_id
      errors.add(:user_id, "não pode ter mais que uma conta")
      throw(:abort)
    end
  end

  def self.not_have_balance(current_user, value, number)
    return unless current_user
    p "*********"
    p value.to_i
    p current_user.account
    p "aqui num"
    p number
    current_user.account.balance > value && current_user.account.number_from_account != number
  end
end
