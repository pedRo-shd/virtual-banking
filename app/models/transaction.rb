class Transaction < ApplicationRecord
  belongs_to :account

  scope :created_between, lambda { |start_date, end_date, account_user| where("account_id =? AND created_at >= ? AND created_at <= ?", account_user, start_date, end_date ) }
end
