class Transfer < ApplicationRecord
  belongs_to :account

  validates :account_number, presence: true
  validates :value, presence: true
  validates :account_id, presence: true
end
