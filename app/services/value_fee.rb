class ValueFee
  def initialize(obj, account, value_transaction)
    @account_user = Account.where(number_from_account: account.number_from_account)
                           .first
    @value_transaction = value_transaction
    @fee = fee_by_working_days ? fee_by_working_days : fee_by_others_days
    @account_user.balance += @fee - @value_transaction
    @account_user.save
  end

  def fee_by_transaction
    @fee
  end

  private

  def fee_by_working_days
    return (- 5 + greather_than_1000) if verify_wday && verify_hour
  end

  def fee_by_others_days
    return (-7 + greather_than_1000) unless verify_wday && verify_hour
  end

  def verify_wday
    current_day = Time.now.wday
    (0..5).each do |week_days|
      @week_wday = current_day if week_days == current_day
    end
    @week_wday
  end

  def verify_hour
    current_hour = Time.now.hour
    (9..18).each do |hour_week|
      @week_hour = current_hour if hour_week == current_hour
    end
    @week_hour
  end

  def greather_than_1000
    @value_transaction >= 1000 ? -10 : 0
  end
end
