class TransactionAccount

  def initialize(obj, value, account)
    if obj.class == Deposit
      @obj_deposit = true
      @value_deposit = value
    end

    if obj.class == Transfer
      @obj_transfer = true
      @value_transfer = value
    end

    if obj.class == Withdrawal
      @obj_withdrawal = true
      @value_withdrawal = value
    end

    cash = Transaction.new(deposit: @obj_deposit, value_deposit: @value_deposit,
                           transfer: @obj_transfer, value_transfer: @value_transfer,
                           withdrawal: @obj_withdrawal, value_withdrawal: @value_withdrawal,
                           account_id: account.id)
    cash.save!
  end

end
