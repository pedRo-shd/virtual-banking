class DepositsController < ApplicationController
  before_action :set_deposit, only: %i[show]
  respond_to :html

  def show
    respond_with @deposit
  end

  def new
    @deposit = Deposit.new
    respond_with @deposit
  end

  def create
    @deposit = Deposit.new(params_deposit)
    @account = Account.to_find_acount(@deposit,
                                      @deposit.account_number,
                                      @deposit.value,
                                      nil)
    raise NotDeposited unless @account
    @deposit.account_id = @account.id
    flash[:notice] = 'Depósito efetuado com sucesso' if @deposit.save
    TransactionAccount.new(@deposit, @deposit.value, @account)
    respond_with @deposit
  end

  private

  def set_deposit
    @deposit = Deposit.find(params[:id])
  end

  def params_deposit
    params.require(:deposit).permit(:name, :account_number, :value, :account_id)
  end
end
