class Accounts::TransfersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_transfer, only: %i[show]
  respond_to :html

  def show
    respond_with @transfer
  end

  def new
    @transfer = Transfer.new
    respond_with @transfer
  end

  def create
    @transfer = Transfer.new(params_transfer)
    @account = Account.to_find_acount(@transfer,
                                      @transfer.account_number,
                                      @transfer.value,
                                      current_user)
    raise NotTransferred unless @account
    from_fee = ValueFee.new(@transfer, current_user.account, @transfer.value)
    @transfer.account_id = @account.id
    flash[:notice] = 'Transferência efetuado com sucesso' if @transfer.save
    TransactionAccount.new(@transfer, @transfer.value, @account)
    respond_with(@transfer, location: accounts_transfer_path(@transfer))
  end

  private

  def set_transfer
    @transfer = Transfer.find(params[:id])
  end

  def params_transfer
    params.require(:transfer).permit(:name, :account_number, :value, :account_id)
  end
end
