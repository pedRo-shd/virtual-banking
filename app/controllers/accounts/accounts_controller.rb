class Accounts::AccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account, only: %i[edit update show]
  respond_to :html

  def index
    @accounts = Account.all
  end

  def balance_account
    account = Account.get_of_balance(current_user.id)
    @account = account.balance
    # respond_with(:accounts, @account)
  end

  def new
    @account = Account.new
    respond_with(:accounts, @account)
  end

  def edit
  end

  def create
    @account = Account.new(params_account)
    flash[:notice] = 'Conta criada com sucesso' if @account.save
    respond_with(:accounts, @account)
  end

  def update
    flash[:notice] = 'Conta atualizada com sucesso' if @account.update(params_account)
    respond_with(:accounts, @account)
  end

  def show
    respond_with(:accounts, @account)
  end

  private

  def set_account
    @account = Account.find(params[:id])
  end

  def params_account
    params.require(:account).permit(:balance, :active, :total,
                                    :number_from_account, :user_id)
  end
end
