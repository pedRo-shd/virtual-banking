class Accounts::SearchController < ApplicationController

  def account_extract
    if params[:q].present?
      time = params[:q].to_time
      q = time.advance(hours: 7)
      time_term = params[:term].to_time
      term = time_term.advance(hours: 23)
      @transaction_from_account = Transaction.created_between(q, term, current_user.account)
    end
  end
end
