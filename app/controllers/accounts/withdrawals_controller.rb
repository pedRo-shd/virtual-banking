class Accounts::WithdrawalsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_withdrawal, only: %i[show]
  respond_to :html

  def show
    respond_with @withdrawal
  end

  def new
    @withdrawal = Withdrawal.new
    respond_with @withdrawal
  end

  def create
    @withdrawal = Withdrawal.new(params_withdrawal)
    raise NotPermited unless verify_withdrawal
    @account = Account.to_find_acount(@withdrawal,
                                      @withdrawal.account_number,
                                      @withdrawal.value,
                                      nil)
    raise NotWithdrawn unless @account
    @withdrawal.account_id = @account.id
    if @withdrawal.save
      TransactionAccount.new(@withdrawal, @withdrawal.value, @account)
      redirect_to accounts_withdrawal_path(@withdrawal), notice: 'Saque efetuado com sucesso'
    else
      render :new
    end
  end

  private

  def verify_withdrawal
    return true unless current_user.account.present?
    @withdrawal.account_number == current_user.account.number_from_account
  end


  def set_withdrawal
    @withdrawal = Withdrawal.find(params[:id])
  end

  def params_withdrawal
    params.require(:withdrawal).permit(:name, :account_number, :value, :account_id)
  end
end
