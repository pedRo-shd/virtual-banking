class ApplicationController < ActionController::Base
  NotDeposited     = Class.new(StandardError)
  NotWithdrawn     = Class.new(StandardError)
  NotTransferred   = Class.new(StandardError)
  NotPermited      = Class.new(StandardError)

  protect_from_forgery with: :exception

  rescue_from NotDeposited, with: :not_deposited
  rescue_from NotWithdrawn, with: :not_withdrawn
  rescue_from NotTransferred, with: :not_transferred
  rescue_from NotPermited, with: :not_permited

  def not_deposited
    flash[:alert] = 'Deposito não efetuado, tente novamente!'
    redirect_to root_path
  end

  def not_withdrawn
    flash[:alert] = 'Saque não efetuado, tente novamente!'
    redirect_to root_path
  end

  def not_transferred
    flash[:alert] = 'Transferência não efetuado, tente novamente!'
    redirect_to root_path
  end

  def not_permited
    flash[:alert] = 'Não é possível sacar valor de outra conta, a menos que seja sua'
    redirect_to root_path
  end
end
