# Virtual Banking

- App web simulando comportamento de caixa eletrônico, possibilitando transferências, saques e depóstitos

Dependências:

* RVM (https://rvm.io/rvm/install)
* Ruby: 2.4.2
* Rails: 5.1.1

Para começar a usar este projeto, siga as intruções abaixo:

* Clone o repsitório: `git clone https://pedRo-shd@bitbucket.org/pedRo-shd/virtual-banking.git`
* Rode

  ```
  bundle install
  ```
  ```
  rails db:create db:migrate
  ```
  ```
  rails s
  ```
